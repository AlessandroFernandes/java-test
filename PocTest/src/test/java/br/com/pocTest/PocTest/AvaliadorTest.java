package br.com.pocTest.PocTest;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertEquals;

import java.util.Random;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import br.com.pocTest.Builder.LeilaoBuilder;

public class AvaliadorTest {

	private Avaliador avaliador;
	private Usuario alessandro;
	private Usuario fernanda;
	private Usuario onix;

	@Before
	public void criarAvaliador()
	{
		avaliador = new Avaliador();
		alessandro = new Usuario("Alessandro");
		fernanda = new Usuario("Fernanda");
		onix = new Usuario("Onix");
	}
	
	@Test
	public void avaliarMaiorEMenorLance()
	{
		Leilao leilao = new LeilaoBuilder()
						  		.criarLeilao("Playstation 5")
						  		.adicionarLance(alessandro, 11000.0)
						  		.adicionarLance(fernanda, 5000.0)
						  		.adicionarLance(onix, 500.00)
						  		.finalizar();
		criarAvaliador();
		avaliador.avaliar(leilao);
		
		assertThat(avaliador.getMaiorLance(), equalTo(11000.0));
		assertEquals(500.00, avaliador.getMenorLance(), 0.00001);
	}
	
	@Test
	public void UnicoLance()
	{
		Leilao leilao = new LeilaoBuilder()
										  	.criarLeilao("Playstation 5")
										  	.adicionarLance(alessandro, 11000.00)
										  	.finalizar();
		
		avaliador.avaliar(leilao);
		
		assertEquals(11000.00, avaliador.getMaiorLance() , 0.00001);
		assertEquals(11000.00, avaliador.getMenorLance(), 0.00001);
	};
	
	@Test(expected = RuntimeException.class)
	public void lanceZerado()
	{
		Leilao leilao = new LeilaoBuilder()
								.criarLeilao("Playstation 5")
								.adicionarLance(alessandro, 0)
								.finalizar();
		
		avaliador.avaliar(leilao);
	};
	
	@Test
	public void LanceRandomico()
	{
		Leilao lancesRandomicos = new LeilaoBuilder()
										.criarLeilao("Playstation 5")
										.adicionarLance(alessandro, new Random().nextDouble())
										.adicionarLance(fernanda, new Random().nextDouble())
										.adicionarLance(onix, new Random().nextDouble())
										.finalizar();
		criarAvaliador();
		avaliador.avaliar(lancesRandomicos);
		
		assertEquals(avaliador.getMaiorLance(), maiorLanceRandomico(avaliador) , 0.00001);
		assertEquals(avaliador.getMenorLance(), menorLanceRandomico(avaliador), 0.00001);
	}
	
	@Test
	public void tresMaioresLances()
	{
		Leilao leilao = new LeilaoBuilder()
								.criarLeilao("Playstation 5")
								.adicionarLance(alessandro, 11000)
								.adicionarLance(fernanda, 5000)
								.adicionarLance(onix, 1500)
								.adicionarLance(alessandro, 13000)
								.adicionarLance(fernanda, 150)
								.finalizar();
		
		criarAvaliador();
		avaliador.avaliar(leilao);
		
		assertThat(avaliador.getLances(), hasItems(
					new Lance(alessandro, 13000),
					new Lance(alessandro, 11000),
					new Lance(fernanda, 5000)
				));
	}
	
	private double maiorLanceRandomico(Avaliador avaliador)
	{
		return avaliador.getMaiorLance() > avaliador.getMenorLance() ? avaliador.getMaiorLance() : Double.NEGATIVE_INFINITY;
	};
	
	private double menorLanceRandomico(Avaliador avaliador)
	{
		return avaliador.getMenorLance() < avaliador.getMaiorLance() ? avaliador.getMenorLance() : Double.POSITIVE_INFINITY;
	};
	
}

