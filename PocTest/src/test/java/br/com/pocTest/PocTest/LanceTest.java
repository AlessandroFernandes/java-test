package br.com.pocTest.PocTest;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import br.com.pocTest.Builder.LeilaoBuilder;

public class LanceTest {
	
	private Usuario alessandro;
	private Usuario fernanda;


	@Before
	public void criarUsuario()
	{
		alessandro = new Usuario("Alessandro");
		fernanda = new Usuario("Fernanda");
	}
	
	@Test
	public void controlaLanceDuplo()
	{
		Usuario alessandro = new Usuario("Alessandro");
		
		Leilao leilao = new LeilaoBuilder()
								.criarLeilao("New car 2020")
								.adicionarLance(alessandro, 4000.00)
								.adicionarLance(alessandro, 5000.0)
								.adicionarLance(alessandro, 3000.00)
								.finalizar();

		assertEquals(1, leilao.getLances().size(), 0.00001);
	};
	
	
	  @Test
	  public void usuarioNaoPodeefetuarMaisQueCincoLances() 
	  {
		  Leilao leilao = new LeilaoBuilder()
				  				.criarLeilao("New car 2020")
				  				.adicionarLance(alessandro, 4000.00)
				  				.adicionarLance(fernanda, 3000.00)
				  				.adicionarLance(alessandro, 9000.00)
				  				.adicionarLance(fernanda, 5000.00)
				  				.adicionarLance(alessandro, 7000.00)
				  				.adicionarLance(fernanda, 4500.00)
				  				.adicionarLance(alessandro, 6000.00)
				  				.adicionarLance(fernanda, 6000.00)
				  				.adicionarLance(alessandro, 9800.00)
				  				.adicionarLance(fernanda, 3000.00)
				  				.adicionarLance(alessandro, 8500.00)
				  				.finalizar();

	  assertEquals(10, leilao.getLances().size(), 0.00001); 
	  }
	 

}
