package br.com.pocTest.PocTest;


public class Usuario {

	private int id;
	private String nome;
	
	public Usuario(String nome) {
		this(0, nome);
	}

	public Usuario(int id, String nome) {
		this.id = id;
		this.nome = nome;
	}

	public int getId() {
		return id;
	}

	public String getNome() {
		return nome;
	}
	
	public boolean equals(Object obj)
	{
		if (obj instanceof Usuario)
		{
			Usuario comparar = (Usuario) obj;
			return this.getNome().equals(comparar.getNome()) && this.getId() == comparar.getId();
		}
		else
		{
			return false;
		}
	}
	
}
