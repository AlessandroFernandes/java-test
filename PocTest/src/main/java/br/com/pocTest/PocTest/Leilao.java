package br.com.pocTest.PocTest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Leilao {

	private String descricao;
	private List<Lance> lances;
	
	public Leilao(String descricao) {
		this.descricao = descricao;
		this.lances = new ArrayList<Lance>();
	}
	
	public void propoe(Lance lance) 
	{
		if(lances.isEmpty() || podeDarLance(lance.getUsuario()))
		{
			lances.add(lance);
		}
	};
		
	public String getDescricao() {
		return descricao;
	}

	public List<Lance> getLances() {
		return Collections.unmodifiableList(lances);
	}
	
	private boolean podeDarLance(Usuario usuario)
	{
		return !ultimoLance(usuario) && numeroDeLancePorUser(usuario);
	};
	
	private boolean ultimoLance(Usuario usuario)
	{
		return lances.get(lances.size() - 1).getUsuario().equals(usuario);
	};
	
	private boolean numeroDeLancePorUser(Usuario usuario)
	{
		int numeroLances = 0;
		
		for(Lance lance : lances)
		{
			if(lance.getUsuario().equals(usuario)) numeroLances++;
		}
		
		return numeroLances < 5 ? true : false;
	};
	
	
}
