package br.com.pocTest.PocTest;

import java.io.ObjectInputStream.GetField;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.management.RuntimeErrorException;

public class Avaliador {

	private double maiorLance = Double.NEGATIVE_INFINITY;
	private double menorLance = Double.POSITIVE_INFINITY;
	private List<Lance> maioresLances;

	public void avaliar(Leilao leilao)
	{
		
		for(Lance lance : leilao.getLances())
		{
			if(lance.getValor() == 0) throw new RuntimeException("Não é possível dar lances zerados!");
			
			if(lance.getValor() > maiorLance) maiorLance = lance.getValor();
			if(lance.getValor() < menorLance) menorLance = lance.getValor();
		}
		
		maioresLances = new ArrayList<Lance>(leilao.getLances());
		
		Collections.sort(maioresLances, new Comparator<Lance>() {
			public int compare(Lance l1, Lance l2)
			{
				if(l1.getValor() < l2.getValor()) return 1;
				if(l1.getValor() > l2.getValor()) return -1;
				return 0;
			}
		});
		
		maioresLances = maioresLances.subList(0, maioresLances.size() > 3 ? 3 : maioresLances.size());
	}
	
	public double getMaiorLance() {
		return maiorLance;
	}
	
	public double getMenorLance() {
		return menorLance;
	}
	
	public List<Lance> getLances()
	{
		return maioresLances;
	}
	
}
