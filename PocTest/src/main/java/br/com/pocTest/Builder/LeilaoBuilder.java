package br.com.pocTest.Builder;

import br.com.pocTest.PocTest.Usuario;
import br.com.pocTest.PocTest.Lance;
import br.com.pocTest.PocTest.Leilao;

public class LeilaoBuilder {
	
	private Leilao leilao;
	private Lance lance;

	public LeilaoBuilder criarLeilao(String item)
	{
		leilao = new Leilao(item);
		return this;
	}
	
	public LeilaoBuilder adicionarLance(Usuario user, double quantia)
	{
		lance = new Lance(user, quantia);
		leilao.propoe(lance);
		return this;
	}
	
	public Leilao finalizar()
	{
		return leilao;
	}

}

